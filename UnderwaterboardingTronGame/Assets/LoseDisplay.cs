﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoseDisplay : MonoBehaviour {
	[SerializeField]
	Player player;
	[SerializeField]
	float disappearTime = 10;
	float disappearTimer;
	void Start() {
		disappearTimer = disappearTime;
		player.Died += () => gameObject.SetActive(true);
		gameObject.SetActive (false);
	}

	void Update() {
		disappearTimer -= Time.deltaTime;
		if (disappearTimer <= 0) {
			disappearTimer = disappearTime;
			gameObject.SetActive (false);
		}
	}
}