﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class DiscButton : MonoBehaviour {
	public abstract void PressButton ();
}
