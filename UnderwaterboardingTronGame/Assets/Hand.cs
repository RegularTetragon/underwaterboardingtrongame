﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Valve.VR;
[RequireComponent(typeof(SteamVR_TrackedObject))]
[RequireComponent(typeof(Rigidbody))]
public class Hand : MonoBehaviour{
	[SerializeField]
	GameObject discPrefab;
	[SerializeField]
	EVRButtonId triggerButton;
	[SerializeField]
	RigidbodyConstraints releasedDiscConstraints;

    // Sounds
    [SerializeField]
    private AudioClip throwDiskSound;

    private SteamVR_TrackedObject trackedObj;

	private SteamVR_Controller.Device controller {
		get {
			return SteamVR_Controller.Input ((int)trackedObj.index);
		}
	}

	/// Bookkeeping
	private GameObject disc;
	private FixedJoint jointOnHeldDisc;
	private Rigidbody rigidbody;
	private Player player;

	private float maximumLockAngle = 30;

	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag ("Player").GetComponent<Player> ();
		trackedObj = GetComponent<SteamVR_TrackedObject> ();
		rigidbody = GetComponent<Rigidbody> ();
        if(throwDiskSound == null)
        {
            Debug.LogWarning("!!!!Add the disk thrown sound to the instance of Hand.cs!!!!");
        }
	}
	
	// Update is called once per frame
	void Update () {
		if (controller == null) {
			print ("Controller is null!");
			return;
		}
		if (player == null) {
			print ("Player is null!");
			return;
		}

		if (controller.GetPressDown (triggerButton) && player.CheckIfDiscAvailable()) {
			disc = Instantiate (discPrefab);
			disc.GetComponent<DiscProjectile> ().ThrowingHand = gameObject;
			disc.transform.position = transform.position;
            disc.GetComponentInChildren<AudioSource>().PlayOneShot(throwDiskSound);
			jointOnHeldDisc = disc.GetComponent<FixedJoint> ();
			jointOnHeldDisc.connectedBody = rigidbody;
		}

		if (controller.GetPressUp (triggerButton) && disc != null) {
			//jointOnHeldDisc.breakForce = 0;
			//jointOnHeldDisc.breakTorque = 0;
			Destroy(jointOnHeldDisc);
			Rigidbody discRb = disc.GetComponent<Rigidbody> ();

			discRb.velocity = controller.velocity;
			//if (Vector3.Angle (controller.velocity, controller.velocity - Vector3.up * controller.velocity.y) < maximumLockAngle) {
				discRb.constraints = releasedDiscConstraints;
			//}
		}
	}
}
