﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionMessage : MonoBehaviour {
	public delegate void CollisionHandler(Collision collision);
	public event CollisionHandler OnCollision;
	void OnCollisionEnter(Collision collision) {
		print ("Collision!");
		if (OnCollision != null) {
			OnCollision (collision);
		}
	}
}
