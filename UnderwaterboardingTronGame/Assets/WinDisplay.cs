﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinDisplay : MonoBehaviour {
	[SerializeField]
	waveManager WaveManager;
	[SerializeField]
	float disappearTime = 10;
	float disappearTimer;
	void Start() {
		disappearTimer = disappearTime;
		WaveManager.Won += () => gameObject.SetActive(true);
		gameObject.SetActive (false);
	}

	void Update() {
		disappearTimer -= Time.deltaTime;
		if (disappearTimer <= 0) {
			disappearTimer = disappearTime;
			gameObject.SetActive (false);
		}
	}
}