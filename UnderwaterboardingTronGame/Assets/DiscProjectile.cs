﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody))]
public class DiscProjectile : MonoBehaviour {
	[SerializeField]
	float force = 1.0f;
	Rigidbody rigidBody;


	[SerializeField]
	float debouceTime = 1;
	[SerializeField]
	float bounceVelocityMultiplier = 2;
	[SerializeField]
	int bouncesUntilReturn = 3;
	[SerializeField]
	float returnTime = 5;
	[SerializeField]
	float lostTime = 15;
	[SerializeField]
	float limitingConstant = 2.0f;
	[SerializeField]
	float returnConstant = 10.0f;

	[SerializeField]
	float addAmmoDistance = .5f;
	[SerializeField]
	AnimationCurve damageVelocityCurve;

	int bounces = 0;
	float debounceTimer = 0;
	bool returning = false;

    [SerializeField]
    private AudioClip hitEnemySound;
    [SerializeField]
    private AudioClip hitOtherSound;
    private AudioSource sound;

	GameObject throwingHand;
	public GameObject ThrowingHand {
		get {
			return throwingHand;
		}
		set {
			throwingHand = value;
		}
	}


	// Use this for initialization
	void Start () {
		rigidBody = GetComponent<Rigidbody> ();
        sound = GetComponentInChildren<AudioSource>();
	}

	void OnCollisionEnter (Collision collision)
	{
		print ("Disc collided with " + collision.collider.name);
		if (debounceTimer <= 0) {
			debounceTimer = debouceTime;
			rigidBody.velocity *= bounceVelocityMultiplier;
			enemy en = collision.gameObject.GetComponent<enemy> ();
			DiscButton discbutton = collision.gameObject.GetComponent<DiscButton>();
			if (en) {
				en.dealDamage ((int)damageVelocityCurve.Evaluate (rigidBody.velocity.magnitude));
                sound.PlayOneShot(hitEnemySound);
			}
			else if (discbutton != null) {
				discbutton.PressButton ();
				returning = true;
			}
			else {
				bounces++;
				print ("Bounces " + bounces);
				if (bounces >= bouncesUntilReturn) {
					returning = true;
				}
                sound.PlayOneShot(hitOtherSound);
			}
		}
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (returning) {
			//Limit velocity to help change direction, and accelerate towards the hand that threw it
			Vector3 velocityOppositeDirection = (rigidBody.velocity.magnitude <= 1) ? rigidBody.velocity : -rigidBody.velocity.normalized;
			Vector3 targetDirection = (throwingHand.transform.position - transform.position).normalized;
			rigidBody.AddForce (velocityOppositeDirection * limitingConstant + targetDirection * returnConstant);
		}
		else {
			rigidBody.AddForce (rigidBody.velocity.normalized * force);
		}
	}

	void Update() {
		lostTime -= Time.deltaTime;
		returnTime -= Time.deltaTime;
		if (returnTime <= 0) {
			returning = true;
		}

		if (lostTime <= 0) {
			//Hard return. Disable collision.
			GetComponentInChildren<Collider>().enabled = false;
		}

		if (returning && (transform.position - throwingHand.transform.position).magnitude < addAmmoDistance) {
			GameObject.FindGameObjectWithTag ("Player").GetComponent<Player> ().ReturnDisc ();
			Destroy (gameObject);
		}
		debounceTimer -= Time.deltaTime;
	}


}
