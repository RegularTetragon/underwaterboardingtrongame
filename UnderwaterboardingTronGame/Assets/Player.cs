﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour {
    int health = 100;
	int ammo = 3;
    [SerializeField]
    private AudioClip playerDeathSound;
    [SerializeField]
    private AudioClip tookDamageSound;
    [SerializeField]
    private AudioClip waveDing;
	[SerializeField]
	waveManager WaveManager;
	[SerializeField]
	Text healthText;
	[SerializeField]
	float gameRestartTime = 10;
	float gameRestartTimer;
	bool reloading = false;

	public delegate void DiedHandler();
	public event DiedHandler Died;
    

    private AudioSource sound;
	// Use this for initialization
	void Start () {
        sound = GetComponentInChildren<AudioSource>();
		WaveManager.WaveBegan += playWaveDingSound;
		gameRestartTimer = gameRestartTime;
	}

	
	// Update is called once per frame
	void Update () {
		if (reloading) {
			gameRestartTimer -= Time.deltaTime;
			SceneManager.LoadScene (0);
		}
	}

	public bool CheckIfDiscAvailable() {
		if (ammo > 0) {
			ammo--;
			return true;
		}
		return false;
	}

	public void ReturnDisc() {
		ammo++;
	}

	public void DealDamage(int amt) {
        health -= amt;
        sound.PlayOneShot(tookDamageSound);
        updateHealth();
	}

    private void updateHealth()
    {
		healthText.text = "Health " + health.ToString().PadLeft(3);
        if(health <= 0)
        {
            die();
        }
    }

    private void die()
    {
        sound.PlayOneShot(playerDeathSound);
		reloading = true;
		if (Died != null) {
			Died ();
		}
    }

	public void playWaveDingSound (int waveNumber)
    {
        sound.PlayOneShot(waveDing);
    }
}
