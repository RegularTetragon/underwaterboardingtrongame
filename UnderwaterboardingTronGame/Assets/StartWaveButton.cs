﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartWaveButton : DiscButton {
	[SerializeField]
	waveManager WaveManager;
	GameObject graphicsToDeactivate;
	void Start() {
		WaveManager.WaveBegan += Hide;
		WaveManager.WaveEnded += Show;
	}

	void Hide (int WaveNumber)
	{
		gameObject.SetActive (false);
	}

	void Show ()
	{
		gameObject.SetActive (true);
	}

	public override void PressButton() {
		WaveManager.StartWave ();
	}
}
