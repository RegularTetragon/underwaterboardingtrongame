﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class baby : enemy
{
    [SerializeField]
    int damage;
    
    private void OnControllerColliderHit(ControllerColliderHit collision)
    {
        GameObject hit = collision.gameObject;
        if(hit.tag.Equals("Player"))
        {
            hit.GetComponent<Player>().DealDamage(damage);
            kill();
        }        
    }

    // Phase
    public override void AttackPlayer()
    {
        if (setState()) return;
        attackMovement();
    }

    // Function
    public override void alertPlayer()
    {

    }

    public override void dealDamage(int amt)
    {
        Health -= amt;
    }

    public override void kill()
    {
		base.kill ();
        Instantiate(gib, transform.position, transform.rotation);
        Destroy(gameObject);
    }

    private float angle;
    public override void attackMovement()
    {
        Vector3 idealPosition = Vector3.MoveTowards(transform.position, Target.transform.position, 1.0f);
        Vector3 difference = new Vector3(idealPosition.x - transform.position.x, transform.position.y, idealPosition.z - transform.position.z);

        CC.SimpleMove(difference * LinearSpeed * 3);
    }

    public override void LeaveDangerZone()
    {
        Vector3 idealPosition = Vector3.MoveTowards(transform.position, Target.transform.position, 1.0f);
        Vector3 difference = new Vector3(idealPosition.x - transform.position.x, transform.position.y, idealPosition.z - transform.position.z);

        CC.SimpleMove(difference * LinearSpeed * -1);
    }

    // This is running into the player
    public override void shoot()
    {

    }
}
