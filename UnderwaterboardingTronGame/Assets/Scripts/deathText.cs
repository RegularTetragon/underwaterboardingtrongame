﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[RequireComponent(typeof(Text))]
public class deathText : MonoBehaviour {
    [SerializeField]
    private float lifespan;
    [SerializeField]
    private float fadeTime;
    [SerializeField]
    private float maxHeight;
    private float speed;
    private float lifetime = 0;


	// Use this for initialization
	void Start () {
        speed = maxHeight / (lifespan - fadeTime);
	}
	
	// Update is called once per frame
	void Update () {
        Text t = GetComponent<Text>();
        lifetime += Time.deltaTime;
        if (lifetime > lifespan - fadeTime)
        {
            t.color = new Color(t.color.r, t.color.b, t.color.g, t.color.a - ((1 / lifespan) * Time.deltaTime));
            if (t.color.a <= 0)
            {
                Destroy(gameObject.transform.parent.transform.parent.gameObject);
            }
        }
        else
        {
            float distance = speed * Time.deltaTime;
            transform.position = new Vector3(transform.position.x, transform.position.y + distance, transform.position.z);
        }
	}
}
