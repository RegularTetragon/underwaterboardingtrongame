﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class waveManager : MonoBehaviour {

    [SerializeField]
    private GameObject[] spawners;

    [Serializable]
    public struct Wave {
        public List<Subwave> subwaves;
    }

    [Serializable]
    public struct Subwave{
        public List<GameObject> monstersToSpawn;
        public float timeUntilNextSubwave;
    }
    [SerializeField]
    List<Wave> level = new List<Wave>();

    void Start () {
        
    }

	public delegate void WaveEndHandler();
	public event WaveEndHandler WaveEnded;

	public delegate void WaveBeginHandler(int waveNumber);
	public event WaveBeginHandler WaveBegan;

	public delegate void WonGameHandler();
	public event WonGameHandler Won;

	private bool inWave = false;
	private float subwaveTimer = 0;
	private int currentWave = -1;
	private int currentSubwave = 0;
	private int currentSpawner;
	private int livingMonsters = 0;

	public void StartWave() {
		currentSubwave = 0;
		currentWave++;
		inWave = true;
		if (WaveBegan != null) {
			WaveBegan (currentWave);
		}
	}

	public void Reset() {
		inWave = false;
		subwaveTimer = 0;
		currentWave = -1;
		currentSubwave = 0;
		currentSpawner = 0;
		livingMonsters = 0;
	}
    void Update () {
		print ("Wave: " + currentWave + " Subwave: " + currentSubwave + " Living Monsters: " + livingMonsters);
		if (inWave) {
			//Checks that there are subwaves to spawn
			if (currentSubwave < level [currentWave].subwaves.Count) {
				subwaveTimer -= Time.deltaTime;
				if (subwaveTimer <= 0) {
					//Cycle to the next subwave and make a note of it
					Subwave selectedSubwave = level [currentWave].subwaves [currentSubwave++];
					//Reset the subwave
					subwaveTimer = selectedSubwave.timeUntilNextSubwave;
					List<GameObject> monstersToSpawn = selectedSubwave.monstersToSpawn;
					//While there are monsters in the list
					while (monstersToSpawn.Count > 0) {
						//Create a monster
						GameObject monster = Instantiate (monstersToSpawn [0]);
						//Remove it from the list of monsters to spawn
						monstersToSpawn.RemoveAt (0);
						//Move the monster to the spawner
						monster.transform.position = spawners [(currentSpawner++) % spawners.Length].transform.position;
						//Count the monster
						livingMonsters++;
						//Subscribe monster OnKill event to subtract from livingMonsters
						monster.GetComponent<enemy> ().OnKill += () => {
							livingMonsters--;
						};
					}
				}
			}
				//If you're out of subwaves
				else {
					//Check if there are any living monsters
					if (livingMonsters <= 0) {
						if (WaveEnded != null) {
							WaveEnded ();
						}
						inWave = false;
					}

					if (currentWave + 1 >= level.Count) {
						if (Won != null) {
							Won ();
						}
					}
				}
			}
		}
	}
