﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(CharacterController))]
public class projectileMotion : MonoBehaviour {

    [SerializeField]
    private float speed;
    [SerializeField]
    private int damage;

    public void Start()
    {
        Rigidbody rb = GetComponent<Rigidbody>();
        GameObject target = GameObject.FindGameObjectWithTag("Player");
        rb.velocity = (target.transform.position - transform.position).normalized * speed;
    }

    public void OnCollisionEnter(Collision collision)
    {
        GameObject hit = collision.gameObject;
        if (hit.tag.Equals("Player"))
        {
            hit.GetComponent<Player>().DealDamage(damage);
        }
        Destroy(gameObject);
    }
}
