﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(CharacterController))]
public abstract class enemy : MonoBehaviour {
    // Constants
    public const int SPAWNING = 0, FINDING_PLAYER = 1, ATTACKING_PLAYER = 2, LEAVING_DANGERZONE = 3;
    public const int BASE_POINT_VALUE = 10;
    public const int MAX_POINTS = 30;

    // Properties    
    public float LinearSpeed {
        get
        {
            return linearSpeed;
        }
        set
        {
            linearSpeed = value;
        }
    }
    public float CircularSpeed
    {
        get
        {
            return circularSpeed;
        }
        set
        {
            circularSpeed = value;
        }
    }
    public int Health {
        get
        {
            return health;
        }
        set
        {
            health = value;
        }
    }
    public float AttackRange {
        get
        {
            return attackRange;
        }
    }
    public float DangerZoneRange
    {
        get
        {
            return dangerZoneRange;
        }
    }
   
    public GameObject Projectile
    {
        get
        {
            return projectile;
        }
    }
    public float ProjectileFireRate
    {
        get
        {
            return projectileFireRate;
        }
    }
    public Vector3 RelativeProjectileLocation
    {
        get
        {
            return relativeProjectileLocation;
        }
    }
    public float TimeSinceLastFire
    {
        get;
        set;
    }

    [SerializeField]
    private float linearSpeed;
    [SerializeField]
    private float circularSpeed;
    [SerializeField]
    private int health;
    [SerializeField]
    private float attackRange;
    [SerializeField]
    private float dangerZoneRange;

    [SerializeField]
    private GameObject projectile;
    [SerializeField]
    private float projectileFireRate;
    [SerializeField]
    private Vector3 relativeProjectileLocation;

    [SerializeField]
    private GameObject deathText;
    [SerializeField]
    private float deathTextOffset;

    [SerializeField]
    protected GameObject gib;
    [SerializeField]
    protected AudioClip alertSound;
    protected AudioSource sound;


	public delegate void KilledHandler();
	public event KilledHandler OnKill;

    public GameObject Target
    {
        get
        {
            return GameObject.FindGameObjectWithTag("Player");
        }
    }
    public CharacterController CC
    {
        get
        {
            return GetComponent<CharacterController>();
        }
    }
    public int state { get; set; }
    [SerializeField]
    private float dangerZoneTimeOut = 1;

    private float timeSinceDangerZoneExit = 0;
    private float lifespan = 0;
    // Handle states
    void Update()
    {
        switch (state)
        {
            case SPAWNING:
                break;
            case FINDING_PLAYER:
                FindPlayer();
                break;
            case ATTACKING_PLAYER:
                AttackPlayer();
                break;
            case LEAVING_DANGERZONE:
                LeaveDangerZone();
                break;
        }

        if(Health <= 0)
        {
            GameObject dt = Instantiate(deathText);
            dt.transform.position = new Vector3(transform.position.x, deathTextOffset, transform.position.z);
            dt.GetComponentInChildren<Text>().text = scoreManager.Singleton.addPoints(getPoints()).ToString();
            kill();
        }

        lifespan += Time.deltaTime;
        timeSinceDangerZoneExit += Time.deltaTime;
    }

    // Phases
    void Start()
    {
        sound = GetComponentInChildren<AudioSource>();

        state = SPAWNING;
        alertPlayer();
        state = FINDING_PLAYER;
        TimeSinceLastFire = ProjectileFireRate;
    }

    public void FindPlayer ()
    {
        if (setState()) return;

        /** Move towards target **
         * If you average the two vectors that are the distance between the player and the enemy,
         * and you make the magnitude 1 that is the result of difference
         */    
         //(r2-r1).normalized * speed
         //(r2-r1).magnitude
        Vector3 idealPosition = Vector3.MoveTowards(transform.position, Target.transform.position, 1.0f);
        Vector3 difference = new Vector3(idealPosition.x - transform.position.x, transform.position.y, idealPosition.z - transform.position.z);
        
        CC.SimpleMove(difference * LinearSpeed);   
    }

    public abstract void AttackPlayer();

    public abstract void LeaveDangerZone();

    // Functions
    public abstract void alertPlayer();

	public virtual void kill() {
		if (OnKill != null) {
			OnKill ();
		}
	}
    public abstract void dealDamage(int amt);

    public abstract void attackMovement();
    public abstract void shoot();

    public int getPoints()
    {
        if (lifespan >= MAX_POINTS) return 1;
        return (int)(MAX_POINTS - lifespan);
    }

    public bool setState()
    {
        float distanceFromTarget = Mathf.Abs((transform.position - Target.transform.position).magnitude);
        if (distanceFromTarget < DangerZoneRange)
        {
            if (state == LEAVING_DANGERZONE)
            {
                return false;
            }                
            state = LEAVING_DANGERZONE;
            timeSinceDangerZoneExit = 0;
            return true;
        }        
        if(distanceFromTarget < AttackRange && distanceFromTarget > DangerZoneRange && timeSinceDangerZoneExit > dangerZoneTimeOut)
        {
            if (state == ATTACKING_PLAYER)
            {
                return false;
            }
            state = ATTACKING_PLAYER;
            return true;
        }
        if (distanceFromTarget > AttackRange)
        {
            if (state == FINDING_PLAYER)
            {
                return false;
            }
            state = FINDING_PLAYER;
            return true;
        }
        return false;
    }
}
