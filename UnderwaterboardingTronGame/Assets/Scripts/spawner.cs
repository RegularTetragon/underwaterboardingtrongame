﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawner : MonoBehaviour {
    public void spawn(GameObject prefab)
    {
        Instantiate(prefab).transform.position = transform.position;
    }
}
