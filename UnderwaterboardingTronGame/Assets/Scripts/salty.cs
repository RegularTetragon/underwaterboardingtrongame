﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class salty : enemy {


    // Phase
    public override void AttackPlayer()
    {
        TimeSinceLastFire += Time.deltaTime;
        if (TimeSinceLastFire >= ProjectileFireRate)
        {
            shoot();
            TimeSinceLastFire = 0.0f;
        }
        attackMovement();
    }

    public override void LeaveDangerZone()
    {
        if(setState()) return;
        Vector3 idealPosition = Vector3.MoveTowards(transform.position, Target.transform.position, 1.0f);
        Vector3 difference = new Vector3(idealPosition.x - transform.position.x, transform.position.y, idealPosition.z - transform.position.z);
        
        CC.SimpleMove(difference * CircularSpeed * -1);
    }

    // Function
    public override void alertPlayer()
    {
        sound.PlayOneShot(alertSound);
    }

    public override void dealDamage(int amt)
    {
        Health -= amt;
    }

    public override void kill()
    {
		base.kill ();
        Instantiate(gib, transform.position, transform.rotation);
        Destroy(gameObject);
    }

    private float angle;
    public override void attackMovement()
    {
        if (setState()) return;
        Vector3 towardsPlayer = (Target.transform.position - transform.position).normalized / CircularSpeed;
        Vector3 leftMotion = normalVector(towardsPlayer).normalized * CircularSpeed;

        CC.SimpleMove(towardsPlayer + leftMotion);
    }

    private Vector3 normalVector(Vector3 original)
    {
        return (new Vector3(-1 *original.z, original.y, original.x));
    }

    public override void shoot()
    {
        Vector3 projectileLocation = transform.position + RelativeProjectileLocation;
        GameObject.Instantiate(Projectile, projectileLocation, transform.rotation);
    }
}
