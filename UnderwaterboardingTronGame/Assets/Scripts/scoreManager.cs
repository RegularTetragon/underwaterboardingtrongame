﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scoreManager : MonoBehaviour {
    public static scoreManager Singleton {
        get; private set;
    }

    double score = 0.0;
	[SerializeField]
	Text scoreText;
    public int GlobalScoreMultiplier
    {
        get
        {
            return globalScoreMultiplier;
        }
    }
    private int globalScoreMultiplier = 1;


    void Start () {
        Singleton = this;
        updateScore();
	}

    public int addPoints(int amt)
    {
        score += amt * globalScoreMultiplier;
        updateScore();
        return amt * globalScoreMultiplier;
    }
    
    public void addToGlobalMultiplier(int amt)
    {
        globalScoreMultiplier += Mathf.Abs(amt);
    }

    void updateScore()
    {
		scoreText.text = "Score " + score.ToString().PadLeft(4);
    }
}
